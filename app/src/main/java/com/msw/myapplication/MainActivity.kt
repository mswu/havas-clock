package com.msw.myapplication

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.msw.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), DragViewListener.DragEventHandler {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val clockFragment: ClockFragment get() = supportFragmentManager.findFragmentById(R.id.clock) as ClockFragment
    private val drawerFragment: DrawerFragment get() = supportFragmentManager.findFragmentById(R.id.drawer) as DrawerFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModelStore.clear()
    }

    override fun onBegin(view: View?) {
        drawerFragment.setOpened(true)
    }

    override fun onMove(view: View?) {

    }

    override fun onEnd(view: View?) {
        val hit = hitTest(view!!, drawerFragment.requireView())
        val clockFragmentBottom = clockFragment.requireView().y + clockFragment.requireView().height * 0.75
        if (!hit || drawerFragment.requireView().y > clockFragmentBottom) {
            drawerFragment.setOpened(false)
            resetClockFragment()
        }
    }

    private fun hitTest(v1: View, v2: View): Boolean {
        val r1 = Rect(v1.left, v1.top, v1.right, v1.bottom)
        val r2 = Rect(v2.left, v2.top, v2.right, v2.bottom)
        return r1.intersect(r2)
    }

    private fun resetClockFragment() {
        val params = clockFragment.requireView().layoutParams as FrameLayout.LayoutParams
        params.topMargin = 0
        params.leftMargin = 0
        clockFragment.requireView().layoutParams = params
    }
}