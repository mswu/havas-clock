package com.msw.myapplication

import android.animation.ValueAnimator
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.msw.myapplication.databinding.FragmentDrawerBinding
import kotlin.math.roundToInt

class DrawerFragment : Fragment() {
    private var _binding: FragmentDrawerBinding? = null
    private val binding get() = _binding!!
    private var opened = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentDrawerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setOnClickListener {
            setOpened(!opened)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun setOpened(open: Boolean) {
        opened = open
        val height = dpToPx(
            resources.getDimension(
                if (opened) R.dimen.drawer_open_height else R.dimen.drawer_close_height
            )
        )
        val animator = ValueAnimator.ofInt(requireView().measuredHeight, height)
        animator.addUpdateListener { valueAnimator ->
            val params = requireView().layoutParams
            params.height = valueAnimator.animatedValue as Int
            requireView().layoutParams = params
        }
        animator.duration = 100
        animator.start()
    }

    private fun dpToPx(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).roundToInt()
}