package com.msw.myapplication

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.msw.myapplication.model.DateTime
import kotlinx.coroutines.*

class ClockViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val CLOCK_END_POINT = "https://dateandtimeasjson.appspot.com/"
        private val TAG = ClockViewModel::class.java.simpleName
    }

    private val queue = Volley.newRequestQueue(application)
    private val gson = Gson()
    private var scope: CoroutineScope? = null

    val time = MutableLiveData<String?>()

    override fun onCleared() {
        super.onCleared()
        stop()
    }

    fun start() {
        scope = CoroutineScope(Dispatchers.IO)
        scope?.launch {
            while (true) {
                val request = StringRequest(CLOCK_END_POINT, { response ->
                    val dateTime = gson.fromJson(response, DateTime::class.java)
                    time.value = dateTime.toString()
                }, { error ->
                    Log.w(TAG, error)
                    time.value = null
                })
                queue.add(request)
                delay(1000)
            }
        }
    }

    fun stop() {
        scope?.cancel()
        queue.cancelAll(TAG)
    }
}