package com.msw.myapplication

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.msw.myapplication.databinding.FragmentClockBinding

class ClockFragment : Fragment() {
    private var _binding: FragmentClockBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ClockViewModel by viewModels()
    private var listener: DragViewListener.DragEventHandler? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DragViewListener.DragEventHandler) {
            listener = context
        } else {
            throw RuntimeException("$context must implement DragViewListener.DragEventHandler")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentClockBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.animation = AnimationUtils.loadAnimation(requireContext(), R.anim.rotate_clockwise)
        view.setOnTouchListener(DragViewListener().apply {
            setDelegate(listener)
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.time.observe(viewLifecycleOwner) {
            binding.timeTextView.text = it
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.start()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}