package com.msw.myapplication

import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import java.lang.ref.WeakReference
import kotlin.math.roundToInt

class DragViewListener : View.OnTouchListener {
    private var x = 0f
    private var y = 0f
    private var delegate: WeakReference<DragEventHandler>? = null

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        val params = v?.layoutParams as? FrameLayout.LayoutParams
        return when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                x = event.rawX - params!!.leftMargin
                y = event.rawY - params.topMargin
                delegate?.get()?.onBegin(v)
                true
            }
            MotionEvent.ACTION_UP -> {
                delegate?.get()?.onEnd(v)
                true
            }
            MotionEvent.ACTION_MOVE -> {
                params!!.leftMargin = (event.rawX - x).roundToInt()
                params.topMargin = (event.rawY - y).roundToInt()
                v.layoutParams = params
                v.invalidate()
                delegate?.get()?.onMove(v)
                true
            }
            else -> false
        }
    }

    fun setDelegate(handler: DragEventHandler?) {
        delegate = handler?.let {
            WeakReference(handler)
        }
    }

    interface DragEventHandler {
        fun onBegin(view: View?)
        fun onMove(view: View?)
        fun onEnd(view: View?)
    }
}