package com.msw.myapplication.model

import java.text.SimpleDateFormat
import java.util.*

class DateTime(private val datetime: String) {

    override fun toString(): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-DD hh:mm:ss.SSSSSS", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)

        return try {
            inputFormat.parse(datetime)?.let {
                outputFormat.format(it)
            } ?: ""
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }
}